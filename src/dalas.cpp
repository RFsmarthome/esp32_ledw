#include <Arduino.h>

#include <SerialDebug.h>

#include <OneWire.h>
#include <DallasTemperature.h>

#include "dalas.h"

const int dalasOneWireGpio = 25;

OneWire dalasOneWire(dalasOneWireGpio);
DallasTemperature dalasSensors(&dalasOneWire);

void dalasInit()
{
	dalasSensors.begin();
	dalasSensors.setResolution(12);
}

float dalasReadTemp(uint8_t deviceId)
{
	printlnV("Dalas temp request temp");
	dalasSensors.requestTemperatures();
	printlnV("Dalas temp done");
	float temp = dalasSensors.getTempCByIndex(deviceId);
	debugD("Temp read: %f", temp);
	if(temp != DEVICE_DISCONNECTED_C) {
		printlnE("Device disconnected");
	}
	return temp;
}
