#include <Arduino.h>

#include <SerialDebug.h>

#include <WiFi.h>
#include <AsyncMqttClient.h>
#include <StringSplitter.h>

#include "com.h"
#include "mqtt.h"
#include "main.h"


WiFiClient wifiClient;

String mqttHostname;
String mqttBasePath;
Com *mqttComQueue;

TimerHandle_t mqttTimerHandle;
AsyncMqttClient mqttClient;


void mqttOnConnect(bool sessionPresent) {
	xTimerStop(mqttTimerHandle, 0);
	printlnI("MQTT server connected");

	debugD("Subscribe to: %s", (mqttBasePath + "/cmnd/+/DIM").c_str());
	mqttClient.subscribe((mqttBasePath + "/cmnd/+/DIM").c_str(), 1);

	debugD("Subscribe to: %s", (mqttBasePath + "/cmnd/+/GAMMA").c_str());
	mqttClient.subscribe((mqttBasePath + "/cmnd/+/GAMMA").c_str(), 1);
}

void mqttOnDisconnect(AsyncMqttClientDisconnectReason reason) {
	debugI("Disconnect reason: %d", (int)reason);
	printlnI("Start timer to connect to MQTT server");
	xTimerStart(mqttTimerHandle, 0);
}

void mqttOnPublish(uint16_t packetId) {
	debugD("Publish acknowledged (%d)", packetId);
}

void mqttOnMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{
	char buffer[128];
	strncpy(buffer, payload, len);
	buffer[len] = 0;

	printlnD("Received message from MQTT");
	debugD("Topic: %s", topic);
	debugD("Payload: %s", buffer);

	dim_t cdim;

	String sTopic(topic);
	StringSplitter *splitter = new StringSplitter(sTopic, '/', 5);
	cdim.channel = splitter->getItemAtIndex(3).toInt() - 1;

	if(splitter->getItemAtIndex(4).compareTo("GAMMA") == 0) {
		debugD("Gamma Channel: %d", cdim.channel);
		delete splitter;

		String sPayload(buffer);
		float gamma = sPayload.toFloat();

		debugD("Gamma Value: %f", gamma);
		setGamma(cdim.channel, gamma);
	} else {
		debugD("DIM Channel: %d", cdim.channel);
		delete splitter;

		String sPayload(buffer);
		uint8_t dim = sPayload.toInt();
		if(dim<101) {
			cdim.dim = dim;

			printlnV("Create DIM packet and send on bus");
			mqttComQueue->send(&cdim, 200 / portTICK_PERIOD_MS);
		}
	}
}

void mqttTimerCallback()
{
	if(WiFi.isConnected()) {
		if(!mqttClient.connected()) {
			printlnI("Connecting to MQTT server...");
			mqttClient.connect();
		}
	}
}


void mqttSetup(String &hostname, String &server, String &port, String &basePath, Com *com) {
	mqttComQueue = com;

	mqttBasePath = basePath + "/" + String((long unsigned int)ESP.getEfuseMac(), HEX);

	mqttClient.onDisconnect(mqttOnDisconnect);
	mqttClient.onConnect(mqttOnConnect);
	mqttClient.onPublish(mqttOnPublish);
	mqttClient.onMessage(mqttOnMessage);

	mqttClient.setServer(server.c_str(), port.toInt());

	mqttTimerHandle = xTimerCreate("mqttTimer", pdMS_TO_TICKS(10000), pdTRUE, NULL, reinterpret_cast<TimerCallbackFunction_t>(mqttTimerCallback));
	xTimerStart(mqttTimerHandle, 0);
}

bool mqttSendDIM(uint8_t channel, uint8_t dim)
{
	String topic = mqttBasePath + "/stat/" + String(channel) + "/DIM";
	String payload = String(dim);
	debugV("Publish on %s with value %s", topic.c_str(), payload.c_str());
	uint16_t rc = mqttClient.publish(topic.c_str(), 1, false, payload.c_str(), payload.length());
	return(rc>0);
}

bool mqttSendDalas(uint8_t sensor, float temp)
{
	String topic = mqttBasePath + "/sens/" + String(sensor) + "/TEMP";
	String payload = String(temp);
	debugV("Publish on %s with value %s", topic.c_str(), payload.c_str());
	uint16_t rc = mqttClient.publish(topic.c_str(), 1, false, payload.c_str(), payload.length());
	return(rc>0);
}
