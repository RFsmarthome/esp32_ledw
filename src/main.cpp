#include <Arduino.h>
#include <ArduinoJson.h>

#include <WiFi.h>
#include <WebServer.h>
#include <AutoConnect.h>
#include <ESPmDNS.h>
#include <SPI.h>
#include <SPIFFS.h>

//#include <AsyncMqttClient.h>

#include <SerialDebug.h>

#include "HTTPUpdateServer.h"

#include "driver/ledc.h"

#include "com.h"
#include "mqtt.h"
#include "dalas.h"


#define CHANNELS 1
#define TEMP_DELAY 60

Com *comQueue;

auto ledChannel = new ledc_channel_t[CHANNELS];
auto ledTimer = new ledc_timer_t[CHANNELS];
auto ledGpio = new uint8_t[CHANNELS];
volatile auto ledGamma = new float[CHANNELS];

const char thingName[] = "esp32";
const char thingApPasswort[] = "start1234";

String mqttPublishBase("rfcentral");
String mqttServer;
String mqttPort;

const char *taskName ="flash";
WebServer webServer;
AutoConnect portal(webServer);
HTTPUpdateServer httpUpdateServer;
AutoConnectAux update("/update", "Update");

static const char AUX_mqtt_setting[] PROGMEM = R"raw(
	[
		{
			"title": "MQTT Settings",
			"uri": "/mqtt",
			"menu": "true",
			"element": [
				{
					"name": "caption",
					"type": "ACText",
					"value": "MQTT Settings page"
				},
				{
					"name": "mqttserver",
					"type": "ACInput",
					"value": "",
					"label": "Server",
					"placeholder": "MQTT broker server"
				},
				{
					"name": "mqttport",
					"type": "ACInput",
					"value": "",
					"label": "Port",
					"placeholder": "MQTT Port of the broker server"
				},
				{
					"name": "save",
					"type": "ACSubmit",
					"value": "Save",
					"uri": "/mqtt_save"
				}
			]
		},
		{
			"title": "MQTT Settings saved",
			"uri": "/mqtt_save",
			"menu": "false",
			"element": [
				{
					"name": "caption",
					"type": "ACText",
					"value": "Parameters saved"
				}
			]
		}
	]
)raw";



void getParameters(AutoConnectAux &aux) {
	mqttServer = aux["mqttserver"].value;
	mqttServer.trim();

	mqttPort = aux["mqttport"].value;
	mqttPort.trim();
}

String loadParameters(AutoConnectAux &aux, PageArgument &args)
{
	File param = SPIFFS.open("/param.json", "r");
	if(param) {
		if(aux.loadElement(param)) {
			getParameters(aux);
			printlnA("Parameters loaded");
		} else {
			printlnA("Error by loading parameters");
		}
		param.close();
	} else {
		printlnA("Error by opening parameter file");
	}

	return String("");
}

String saveParameters(AutoConnectAux &aux, PageArgument &args)
{
	AutoConnectAux &mqttSettings = *portal.aux(portal.where());
	getParameters(mqttSettings);

	//AutoConnectInput &mqttserver = mqttSettings["mqttserver"].as<AutoConnectInput>();

	File param = SPIFFS.open("/param.json", "w");
	mqttSettings.saveElement(param, {"mqttserver", "mqttport"});
	param.close();

	return String("");
}

void handleRoot() {
  String  content =
    "<html>"
    "<head>"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
    "</head>"
    "<body>"
    "<div class=\"menu\">" AUTOCONNECT_LINK(BAR_32) "</div>"
	"<p>Build git short HASH "
	GIT_SHORT_HASH
	"</p>"
	"<p>Semantic version: "
	GIT_TAG_VERSION
	"</p>"
	"<p>type: esp32_ledw</p>"
    "</body>"
    "</html>";

  WebServer &webServer = portal.host();
  webServer.send(200, "text/html", content);
}

void setGamma(uint8_t channel, float gamma) {
	ledGamma[channel] = gamma;
}

uint16_t correctGamma(uint16_t res, float gamma, uint16_t color) {
	uint16_t i = (pow((float)color / (float)res, gamma) * res + 0.5);
	//float c = (float)color / (float)res;
	//uint16_t i = res * powf(c, gamma);
	return i;
}


void initPwm() {
	// Define the channels
	ledChannel[0] = LEDC_CHANNEL_0;
	ledTimer[0] = LEDC_TIMER_0;
	ledGpio[0] = 18;
	ledGamma[0] = 2.8;

	ledc_timer_config_t timerConfig;
	timerConfig.duty_resolution = LEDC_TIMER_8_BIT;
	timerConfig.freq_hz = 1000;
	timerConfig.speed_mode = LEDC_HIGH_SPEED_MODE;
	timerConfig.timer_num = LEDC_TIMER_0;
	ledc_timer_config(&timerConfig);

	// Setup PWM channels, 1khz 8bit resolution
	ledc_channel_config_t channelConfig;
	channelConfig.duty = correctGamma(255, 2.2f, 50);
	channelConfig.speed_mode = LEDC_HIGH_SPEED_MODE;
	channelConfig.hpoint = 0;
	for(uint8_t c = 0; c < CHANNELS; ++c) {
		channelConfig.channel = ledChannel[c];
		channelConfig.gpio_num = ledGpio[c];
		channelConfig.timer_sel = ledTimer[c];
		ledc_channel_config(&channelConfig);
	}

	ledc_fade_func_install(0);
}

void setChannelDIM(uint8_t channel, uint8_t dim) {
	debugV("Real Dim: %d, %d", ledChannel[channel], correctGamma(255, ledGamma[channel], dim * 255 / 100));
	ledc_set_duty_and_update(LEDC_HIGH_SPEED_MODE, ledChannel[channel], correctGamma(255, ledGamma[channel], dim), 0);
}

void taskFlash(void *pvParameters) {
	printlnI("Running flash task");
	for(;;) {
		dim_t cdim;
		bool rc = comQueue->receive(&cdim, portMAX_DELAY);
		printlnD("flash task wake up");
		if(rc) {
			printlnD("Receive DIM command");
			if(cdim.channel>=0 && cdim.channel<CHANNELS) {
				debugV("Set Channel DIM: %d (%d)", cdim.channel, cdim.dim);
				setChannelDIM(cdim.channel, cdim.dim);
				mqttSendDIM(cdim.channel, cdim.dim);
			}
		}
	}
}

void taskDalasTemp(void *pvParameters) {
	printlnI("Running temp task");
	for(;;) {
		vTaskDelay(TEMP_DELAY * 1000 / portTICK_PERIOD_MS);
		printlnD("Read ds18x20 device0");
		float temp = dalasReadTemp(0);
		mqttSendDalas(0, temp);
	}
}

void setup() {
	delay(1000);
	Serial.begin(115200);

	SPIFFS.begin(true);

	initPwm();
	setChannelDIM(0, 20);

	printlnA("Start rfcentral...");
	WiFi.enableIpV6();
	String hostname = "esp-"+ String((long unsigned int)ESP.getEfuseMac(), HEX);
	AutoConnectConfig autoConnectConfig; 
	autoConnectConfig.hostName = hostname;
	autoConnectConfig.apid = hostname;
	autoConnectConfig.autoReconnect = true;
	autoConnectConfig.portalTimeout = 60000;
	autoConnectConfig.ticker = false;
	autoConnectConfig.apid = hostname;
	autoConnectConfig.psk = "start1234";
	autoConnectConfig.autoRise = true;
	autoConnectConfig.retainPortal = true;
	autoConnectConfig.autoReset = true;
	autoConnectConfig.autoReconnect = true;
	if(portal.load(FPSTR(AUX_mqtt_setting))) {
		printlnA("Load AUX forms");
		AutoConnectAux &mqttSettings = *portal.aux("/mqtt");
		PageArgument args;
		loadParameters(mqttSettings, args);

		portal.on("/mqtt", loadParameters);
		portal.on("/mqtt_save", saveParameters);
	}
	portal.config(autoConnectConfig);
	httpUpdateServer.setup(&webServer);
	portal.join({update});
	if(portal.begin()) {
		debugA("Wifi connected: %s", WiFi.localIP().toString().c_str());
		MDNS.begin(hostname.c_str());
		MDNS.addService("http", "tcp", 80);
	} else {
		printlnA("Timeout for captive portal. Restart ESP");
		ESP.restart();
		delay(1000);
	}

	setChannelDIM(0, 10);

	comQueue = new Com();
	mqttSetup(hostname, mqttServer, mqttPort, mqttPublishBase, comQueue);

	WebServer &webServer = portal.host();
	webServer.on("/", handleRoot);

	// Disabled for debuging MQTT
	BaseType_t rc = xTaskCreate(taskFlash, (const char*)"flash", configMINIMAL_STACK_SIZE+4096, NULL, tskIDLE_PRIORITY, NULL);
	if(rc==pdPASS) {
		printlnA("pdPASS by xTaskCreate flash");
	}

	dalasInit();
	rc = xTaskCreate(taskDalasTemp, (const char*)"temp", configMINIMAL_STACK_SIZE+4096, NULL, tskIDLE_PRIORITY, NULL);
	if(rc==pdPASS) {
		printlnA("pdPASS by xTaskCreate temp");
	}
}

void loop() {
	debugHandle();
	portal.handleClient();
}
