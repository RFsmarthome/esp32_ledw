#ifndef _MQTT_H
#define _MQTT_H

void mqttSetup(String &hostname, String &server, String &port, String &basePath, Com *com);

bool mqttSendDIM(uint8_t channel, uint8_t dim);
bool mqttSendDalas(uint8_t sensor, float temp);

#endif
